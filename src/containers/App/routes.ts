import HomePage from "../../pages/HomePage";
import MoviesPage from "../../pages/MoviesPage";


export default [
    { path: '/', component: HomePage, title: 'Home' },
    {path: '/movies', component: MoviesPage, title: 'Movies'},
]
