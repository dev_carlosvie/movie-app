import React from "react";
import '../../App.css';
import { Switch, Route } from 'react-router-dom';
import PageNotFound from "../../pages/PageNotFound";
import { ConnectedRouter } from 'connected-react-router';
import history from "../../utils/history";

import routes from "./routes";
import {Provider} from "react-redux";
import appStore from "../../utils/store";

const links = routes.filter(route => Reflect.has(route, 'title'));

function App() {
    return (
        <div className="App">
            <Provider store={appStore}>
                <ConnectedRouter history={history}>
                    <Switch>
                        {routes.map(route => (
                            <Route exact path={route.path} component={route.component} />
                        ))}
                        <Route component={PageNotFound} />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        </div>
    );
}

export default App;
