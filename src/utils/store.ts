import {applyMiddleware, createStore} from "redux";
import {routerMiddleware} from "connected-react-router";
import history from "./history";
import {rootReducer} from "./reducers";

const appStore = createStore(
    rootReducer(history),
    applyMiddleware(
        routerMiddleware(history)
    ),
);

export default appStore;
