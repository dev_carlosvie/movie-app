import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/index';

ReactDOM.render(<App />, document.getElementById('root'));

